# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='django-cadoblog',
    version='0.1.0',
    description='Blog',
    author='Frank Wawrzak (CadoSolutions)',
    author_email='frank.wawrzak@cadosolutions.com',
    url='https://github.com/fsw/django-cadoblog',
    download_url='git://github.com/fsw/django-cadoblog.git',
    packages=['cadoblog'],
    install_requires=[
    ],
    dependency_links = [
    ],
)