from django import forms
from django.forms import fields
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.db.models import ObjectDoesNotExist
from django.shortcuts import get_object_or_404, redirect, render, render_to_response
from django.template import loader, Context, RequestContext
from django.utils.translation import ugettext as _
from django.core.paginator import Paginator, InvalidPage
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404

import urllib

from models import Post, PostCategory, ExternalLink

from haystack.query import SearchQuerySet
from haystack.inputs import AutoQuery
from django.conf import settings

def frontend_context(request): 
    context = {}
    context['categories'] = PostCategory.objects.all()
    context['external_links'] = ExternalLink.objects.all()
    context['title'] = settings.CADO_NAME
    context['search_params'] = {};
    return context

def search(request, category_slug='all'):
    context = {}
    context_instance = RequestContext(request)
    
    if ('category' in request.GET):
        newGET = request.GET.copy()
        newGET.pop('category', None)
        category_slug = request.GET['category']
        return HttpResponseRedirect(reverse('cadoblog.views.search', kwargs={'category_slug':category_slug})
                                            + '?' + urllib.urlencode(newGET));

    results_per_page = 18
    results = SearchQuerySet().facet('category')

    context['search_params'] = {}
    context['breadcrumbs'] = []

    category = PostCategory.objects.get(slug=category_slug)
    context['category'] = category       
    path = category.get_ancestors(include_self=True)
    for category in path:
        context['breadcrumbs'].append((reverse('cadoblog.views.search', kwargs={'category_slug':category.slug}), category.name))
    
    if 'q' in request.GET and request.GET['q']:
        context['search_params']['q'] = request.GET['q']
        results = results.filter(text=AutoQuery(request.GET['q']))
        
    if 'tags' in request.GET and request.GET['tags']:
        context['search_params']['tags'] = request.GET['tags']
        tags = request.GET['tags'].split(',')
        for tag in tags:
            results = results.filter(tags=tag)    

    if context['search_params']:
        urlparams = urllib.urlencode(dict([k, v.encode('utf-8')] for k, v in context['search_params'].items()))
        context['breadcrumbs'].append((
            reverse('cadoblog.views.search', kwargs={'category_slug':category_slug}) + '?' + urlparams
            , 'Search Results'))
    
    context['facet'] = results.facet_counts().get('fields', {})
    
    categories_map = {}
    for category, count in context['facet'].get('category', {}):
        categories_map[category] = count

    new_categories = [];
    for category in context_instance['categories']:
        category.count = categories_map.get(category.slug, 0)
        new_categories.append(category)
        
    context_instance['categories'] = new_categories

    results = results.filter(category=category_slug)
    #results = SearchQuerySet()
    results = results.load_all()
    try:
        page_no = int(request.GET.get('page', 1))
        if (page_no > 1):
            context['breadcrumbs'].append(('', 'Page %d' % page_no))
            
    except (TypeError, ValueError):
        raise Http404("Not a valid number for page.")

    if page_no < 1:
        raise Http404("Pages should be 1 or greater.")

    context['search_params']['page'] = page_no
    context['search_params']['category'] = category_slug

    context['breadcrumbs'][-1] = ('',) + context['breadcrumbs'][-1][1:]

    start_offset = (page_no - 1) * results_per_page
    results[start_offset:start_offset + results_per_page]

    paginator = Paginator(results, results_per_page)

    try:
        page = paginator.page(page_no)
    except InvalidPage:
        raise Http404("No such page!")
    
    context['results'] = results
    context['page'] = page    
    return render_to_response('blog/search.html', context, context_instance)

def index(request):
    context = {};
    return render_to_response('blog/index.html', context, context_instance=RequestContext(request))

def post(request, post_slug):
    context = {};
    context['post'] = get_object_or_404(Post.objects.filter(is_active=True), slug=post_slug)
    
    context['breadcrumbs'] = []
    path = context['post'].category.get_ancestors(include_self=True)
    for category in path:
        context['breadcrumbs'].append((reverse('cadoblog.views.search', kwargs={'category_slug':category.slug}), category.name))

    context['breadcrumbs'].append(('', context['post'].title,))

    #context['related'] = post.related_to.all()
    #context['related'] += post.related.all()
    
    return render_to_response('blog/post.html', context, context_instance=RequestContext(request))