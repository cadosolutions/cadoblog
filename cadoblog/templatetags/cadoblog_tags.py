import urllib, re

from django import template
from django.utils.safestring import mark_safe
from django.template import loader, RequestContext
from django.core.urlresolvers import reverse
from django.forms.forms import Form
from django.forms import fields
from django.db import models

register = template.Library()

from cadoblog.models import PostCategory
from haystack import indexes

@register.simple_tag(name="search_url", takes_context=True)
def get_search_url(context, category=None, tag=None, page=1, view=None):
    
    urlparams = context['search_params'].copy()
    if 'category' in urlparams:
        del urlparams['category']
        
    category_slug = category if category is not None else context['search_params'].get('category', '')
    urlparams['tags'] = tag if tag is not None else context['search_params'].get('tags', None)
    urlparams['page'] = page if page is not None else context['search_params'].get('page', None)
    
    urlparams['view'] = view if view is not None else context['search_params'].get('view', None)

    urlparams = dict([k, str(v).encode('utf-8')] for k,v in urlparams.iteritems() if v is not None)    
    if (urlparams['page'] == '1'):
        del urlparams['page']
    
    if (urlparams.get('view', None) == 'grid'):
        del urlparams['view']
    
    return reverse('cadoshop.views.search', kwargs={'category_slug':category_slug}) + '?' + urllib.urlencode(urlparams)

@register.filter
def urlize_hashtags(value):
    def repl(m):
        hashtag = m.group(1)
        url = reverse('product_list', kwargs={'category_slug':''}) + '?tags=' + hashtag
        return '<a href="%s">&#35;%s</a>' % (url, hashtag)
    hashtag_pattern = re.compile(r'[#]+([-_a-zA-Z0-9]+)')
    return mark_safe(hashtag_pattern.sub(repl, value))

@register.simple_tag(name="search_form_simple", takes_context=True)
def search_form_simple(context):
    
    form = Form(initial=context['search_params'])
    form.fields['q'] = fields.CharField(label = 'search')    
    category_slug = context['search_params'].get('category', 'all')
    category_path = PostCategory.objects.get(slug=category_slug).get_ancestors(include_self=True)
    return loader.render_to_string('blog/search_form_simple.html', 
                                   {'form' : form,
                                    'category_path' : category_path
                                    },
                                   context_instance=RequestContext(context['request']));
