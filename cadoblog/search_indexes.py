from haystack import indexes
from celery_haystack.indexes import CelerySearchIndex
from models import Post
from django.db import models

import re

class PostIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    tags = indexes.MultiValueField(faceted=True)
    category = indexes.MultiValueField(faceted=True)
    has_image = indexes.BooleanField()
    
    def get_model(self):
        return Post
    
    def index_queryset(self, using=None):
        return self.get_model().objects.filter(is_active=True)
    
    def prepare_tags(self, obj):
        return re.findall(r'[#]+([-_a-zA-Z0-9]+)', obj.body)
    
    def prepare_category(self, obj):
        path = obj.category.get_ancestors(include_self=True)
        ret = []
        for category in path:
            ret.append(category.slug)
        return ret
        
    def prepare_has_image(self, obj):
        return bool(obj.image)
    