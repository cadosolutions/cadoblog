from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from mptt.admin import MPTTModelAdmin

from . import models
from imagekit.admin import AdminThumbnail
from django.conf import settings
from django import forms

from django.forms.models import inlineformset_factory
from django.contrib.admin.util import flatten_fieldsets
from django.utils.functional import curry
from cadocms.admin import StaticPageAdmin, Setting, SettingAdmin

class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'admin_thumbnail', 'category', 'is_active')
    list_display_links = ('title', 'admin_thumbnail',)
    list_filter = ('is_active', )
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title', 'body')
    admin_thumbnail = AdminThumbnail(image_field='tiny_thumbnail')
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'category', 'image', 'body', 'is_active')
        }),
    )
    

class PostCategoryAdmin(MPTTModelAdmin):
    
    admin_thumbnail = AdminThumbnail(image_field='thumbnail')
    list_display = ('name', 'admin_thumbnail', 'get_excerpt', 'active')
    list_display_links = ('name',)
    list_filter = ('active', )
    prepopulated_fields = {'slug': ('name',)}
    fieldsets = (
        (None, {
            'fields': ('name', 'slug', 'parent', 'thumbnail', 'description', 'active', )
        }),
        ('SEO', {
            'fields': ('seo_keywords', 'seo_description', 'seo_title',)
        }),
    )
    

admin.site.register(models.ExternalLink)

admin.site.register(models.Post, PostAdmin)
admin.site.register(models.PostCategory, PostCategoryAdmin)

admin.site.register(models.StaticPage, StaticPageAdmin)
admin.site.register(Setting, SettingAdmin)