import sys

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.forms import fields
from django.core.exceptions import ValidationError
from cadocms.models import StaticPage as StaticPageBase
from cadocms.models import Tree, RootedTree, Sluggable, TreeForeignKey
from cadocms.fields import HTMLField
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFit, Adjust, ResizeToFill

class StaticPage(StaticPageBase):
    pass

class PostCategory(RootedTree, Sluggable):

    name = models.CharField(max_length=256, verbose_name=_('name'))
    active = models.BooleanField(default=True, verbose_name=_('active'))
    
    thumbnail = ProcessedImageField([ResizeToFit(50, 50, mat_color=(255, 255, 255))], upload_to='categories', format='JPEG', options={'quality': 90}, blank=True)
    description = models.TextField(blank=True, null=True)
    
    seo_title = models.CharField(max_length=512, blank=True)
    seo_keywords = models.CharField(max_length=512, blank=True,
        help_text="Comma-separated keywords for search engines.")
    seo_description = models.TextField('seo_description', blank=True)#extra_fields = ExtraFieldsDefinition(null=True, blank=True)

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def get_excerpt(self):
        if not self.description:
            return ''
        if len(self.description) > 65:
            return self.description[:65] + '...'
        else:
            return self.description
    get_excerpt.short_description = 'Description'
    
    def __unicode__(self):
        return self.name
        
    @models.permalink
    def get_absolute_url(self):
        return ('cadoblog.views.search', (), {'category_slug': self.slug})
    
class Post(models.Model):
    
    category = TreeForeignKey(PostCategory)
    
    related = models.ManyToManyField('Post', verbose_name='Related Posts', related_name='related_to', blank=True, null=True)
    
    is_active = models.BooleanField(_('is active'), default=True)
    title = models.CharField(_('title'), max_length=100)
    slug = models.SlugField(_('slug'), unique=True)
    body = HTMLField(_('description'), blank=True)

    image = models.ImageField(verbose_name = _('Image'), upload_to='products', blank=True)
    thumbnail = ImageSpecField([ResizeToFit(270, 270)],
                               source='image',
                               format='JPEG', options={'quality': 90})
    tiny_thumbnail = ImageSpecField([ResizeToFit(50, 50)],
                               source='image',
                               format='JPEG', options={'quality': 90})
    

    class Meta:
        #ordering = ['ordering', 'name']
        verbose_name = _('post')
        verbose_name_plural = _('posts')
              
    def __unicode__(self):
        return self.title
    
    @models.permalink
    def get_absolute_url(self):
        return ('cadoblog.views.post', (), {'post_slug': self.slug})
    
class ExternalLink(models.Model):
    
    name = models.CharField(_('name'), max_length=100)
    url = models.URLField()
    
    def __unicode__(self):
        return self.name            
        