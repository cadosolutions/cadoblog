from cadocms.settings import BaseSettings

class Settings(object):
    
    @property
    def INSTALLED_APPS(self): 
        ret= (
                'cadoblog',
        ) + super(Settings, self).INSTALLED_APPS; 
        return ret
    
    @property
    def TEMPLATE_CONTEXT_PROCESSORS(self):
        return super(Settings, self).TEMPLATE_CONTEXT_PROCESSORS + (
               'cadoblog.views.frontend_context',
            )
        
    @property
    def SOLR_CORE_NAME(self):
        return 'blog'