from django.conf.urls import patterns, include, url
from cadocms.urls import urlpatterns
from cadocms.views import staticpage
from . import views

urlpatterns += patterns('',
    url(r'^$', views.index),
    url(r'^s/(?P<category_slug>.*)$', views.search),
    url(r'^p/(?P<post_slug>.*)$', views.post),
    url(r'^(?P<url>.*)$', staticpage, name='staticpage'),
)
